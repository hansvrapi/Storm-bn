Storm 1.6.4

Date: Wed Jun  8 15:08:55 2022
Command line arguments: --jani jani_files/1/survey.jani --prop 'P=? [G(T=0 | T=-1)]'
Current working directory: /home/hans/Desktop/Storm-bn/storm_evidence

Time for model input parsing: 0.001s.

 WARN (FormulaParserGrammar.cpp:328): Identifier `R' coincides with a reserved keyword or operator. Property expressions using the variable or constant 'R' will not be parsed correctly.
 WARN (FormulaParserGrammar.cpp:328): Identifier `S' coincides with a reserved keyword or operator. Property expressions using the variable or constant 'S' will not be parsed correctly.
 WARN (FormulaParserGrammar.cpp:331): Identifier `T' coincides with a reserved keyword or operator. Property expressions using the variable or constant 'T' might not be parsed correctly.
Time for model construction: 0.018s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	19
Transitions: 	41
Reward Models:  none
State Labels: 	3 labels
   * deadlock -> 3 item(s)
   * init -> 1 item(s)
   * ((T = 0) | (T = -(1))) -> 17 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Model checking property "1": P=? [G ((T = 0) | (T = -(1)))] ...
Result (for initial states): 0.5594
Time for model checking: 0.000s.
