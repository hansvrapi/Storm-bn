Storm 1.6.4

Date: Wed Jun  8 15:08:51 2022
Command line arguments: --jani jani_files/no_evidence/child.jani --prop 'P=? [F(LVHreport=1)]'
Current working directory: /home/hans/Desktop/Storm-bn/storm_evidence

Time for model input parsing: 0.007s.

Time for model construction: 0.076s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	1450
Transitions: 	4611
Reward Models:  none
State Labels: 	3 labels
   * deadlock -> 4 item(s)
   * init -> 1 item(s)
   * (LVHreport = 1) -> 6 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Model checking property "1": P=? [F (LVHreport = 1)] ...
Result (for initial states): 0.7133313761
Time for model checking: 0.000s.
