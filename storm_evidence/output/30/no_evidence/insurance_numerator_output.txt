Storm 1.6.4

Date: Wed Jun  8 15:08:54 2022
Command line arguments: --jani jani_files/no_evidence/insurance.jani --prop 'P=? [F(SocioEcon=1|SocioEcon=2|SocioEcon=3|GoodStudent=1|DrivingSkill=1|DrivingSkill=2|AntiTheft=1|Mileage=1|Mileage=2|Mileage=3|Accident=1|Accident=2|Accident=3|Theft=1|MedCost=1|MedCost=2|MedCost=3|PropCost=1|PropCost=2|PropCost=3|ILiCost=1|ILiCost=2|ILiCost=3)]'
Current working directory: /home/hans/Desktop/Storm-bn/storm_evidence

Time for model input parsing: 0.034s.

Time for model construction: 0.044s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	703
Transitions: 	1360
Reward Models:  none
State Labels: 	3 labels
   * deadlock -> 1 item(s)
   * init -> 1 item(s)
   * (((((((((((((((((((((((SocioEcon = 1) | (SocioEcon = 2)) | (SocioEcon = 3)) | (GoodStudent = 1)) | (DrivingSkill = 1)) | (DrivingSkill = 2)) | (AntiTheft = 1)) | (Mileage = 1)) | (Mileage = 2)) | (Mileage = 3)) | (Accident = 1)) | (Accident = 2)) | (Accident = 3)) | (Theft = 1)) | (MedCost = 1)) | (MedCost = 2)) | (MedCost = 3)) | (PropCost = 1)) | (PropCost = 2)) | (PropCost = 3)) | (ILiCost = 1)) | (ILiCost = 2)) | (ILiCost = 3)) -> 239 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Model checking property "1": P=? [F (((((((((((((((((((((((SocioEcon = 1) | (SocioEcon = 2)) | (SocioEcon = 3)) | (GoodStudent = 1)) | (DrivingSkill = 1)) | (DrivingSkill = 2)) | (AntiTheft = 1)) | (Mileage = 1)) | (Mileage = 2)) | (Mileage = 3)) | (Accident = 1)) | (Accident = 2)) | (Accident = 3)) | (Theft = 1)) | (MedCost = 1)) | (MedCost = 2)) | (MedCost = 3)) | (PropCost = 1)) | (PropCost = 2)) | (PropCost = 3)) | (ILiCost = 1)) | (ILiCost = 2)) | (ILiCost = 3))] ...
Result (for initial states): 0.9999999979
Time for model checking: 0.000s.
