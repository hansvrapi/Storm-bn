Storm 1.6.4

Date: Wed Jun  8 15:08:50 2022
Command line arguments: --jani jani_files/5/cancer.jani --prop 'P=? [G(Dyspnoea=0 | Dyspnoea=-1)]'
Current working directory: /home/hans/Desktop/Storm-bn/storm_evidence

Time for model input parsing: 0.001s.

Time for model construction: 0.018s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	9
Transitions: 	16
Reward Models:  none
State Labels: 	3 labels
   * deadlock -> 2 item(s)
   * init -> 1 item(s)
   * ((Dyspnoea = 0) | (Dyspnoea = -(1))) -> 8 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Model checking property "1": P=? [G ((Dyspnoea = 0) | (Dyspnoea = -(1)))] ...
Result (for initial states): 0.3112
Time for model checking: 0.000s.
