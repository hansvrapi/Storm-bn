Storm 1.6.4

Date: Wed Jun  8 15:08:55 2022
Command line arguments: --jani jani_files/5/sachs.jani --prop 'P=? [G(Akt=0 | Akt=-1)]'
Current working directory: /home/hans/Desktop/Storm-bn/storm_evidence

Time for model input parsing: 0.007s.

Time for model construction: 0.019s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	25
Transitions: 	66
Reward Models:  none
State Labels: 	3 labels
   * deadlock -> 3 item(s)
   * init -> 1 item(s)
   * ((Akt = 0) | (Akt = -(1))) -> 23 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Model checking property "1": P=? [G ((Akt = 0) | (Akt = -(1)))] ...
Result (for initial states): 0.2967675289
Time for model checking: 0.000s.
