### run_psdd_storm_comparison.py
	- runs the experiments using psdd and storm for the networks 'andes' and 'win95pts' and outputs a table containing for each pair of method and network the corresponding construction and inference time 
	- takes one boolean argument (optional)
		- 'true': rerun the experiments, parse the results and output them (if not set, the existing results will be parsed)
	- command to run the script: python3 run_psdd_storm_comparison.py [true]  ([] indicates that the argument is optional)

