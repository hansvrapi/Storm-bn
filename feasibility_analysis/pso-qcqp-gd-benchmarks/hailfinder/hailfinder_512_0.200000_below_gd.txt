Storm-pars 1.6.4 (dev)

Date: Fri Dec 31 14:46:17 2021
Command line arguments: --explicit-drn pso-qcqp-gd-benchmarks/hailfinder/hailfinder_512.drn --prop 'Pmin<=0.200000 [F("R5Fcst1"|"Dewpoints1"|"LowLLapse1"|"WindFieldPln1"|"PlainsFcst1")]
' --find-feasible
Current working directory: /Users/bahare/Documents/Experiments/gradient-descent

Time for model construction: 1.020s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	20304
Transitions: 	55976
Reward Models:  none
State Labels: 	225 labels
   * N34StarFcst1 -> 33 item(s)
   * LLIW0 -> 1188 item(s)
   * SubjVertMo0 -> 4 item(s)
   * OutflowFrMt1 -> 96 item(s)
   * QGVertMotion0 -> 16 item(s)
   * Dewpoints4 -> 1 item(s)
   * SubjVertMo1 -> 4 item(s)
   * CombClouds1 -> 16 item(s)
   * RaoContMoist0 -> 4 item(s)
   * RaoContMoist1 -> 4 item(s)
   * N0_7muVerMo1 -> 1 item(s)
   * Scenario8 -> 9 item(s)
   * CombVerMo1 -> 1 item(s)
   * CapChange0 -> 99 item(s)
   * CombMoisture2 -> 4 item(s)
   * AreaMeso_ALS0 -> 1 item(s)
   * ScenRelAMCIN0 -> 54 item(s)
   * WindAloft0 -> 10 item(s)
   * QGVertMotion1 -> 16 item(s)
   * AMDewptCalPl2 -> 99 item(s)
   * MorningBound0 -> 336 item(s)
   * SatContMoist1 -> 16 item(s)
   * ScenRel3_42 -> 9 item(s)
   * AreaMoDryAir0 -> 4 item(s)
   * LoLevMoistAd2 -> 297 item(s)
   * MorningBound2 -> 336 item(s)
   * ScnRelPlFcst10 -> 108 item(s)
   * SatContMoist3 -> 16 item(s)
   * N0_7muVerMo3 -> 1 item(s)
   * CombMoisture1 -> 4 item(s)
   * SubjVertMo2 -> 4 item(s)
   * AMDewptCalPl0 -> 99 item(s)
   * R5Fcst0 -> 11 item(s)
   * CombVerMo2 -> 1 item(s)
   * CombMoisture3 -> 4 item(s)
   * ScenRel3_41 -> 9 item(s)
   * AreaMoDryAir1 -> 4 item(s)
   * Date2 -> 9 item(s)
   * SatContMoist0 -> 16 item(s)
   * TempDis1 -> 11 item(s)
   * AreaMoDryAir3 -> 4 item(s)
   * Boundaries0 -> 144 item(s)
   * ScnRelPlFcst8 -> 108 item(s)
   * SynForcng4 -> 11 item(s)
   * MidLLapse2 -> 10 item(s)
   * Scenario10 -> 9 item(s)
   * SfcWndShfDis3 -> 10 item(s)
   * MidLLapse1 -> 11 item(s)
   * VISCloudCov0 -> 16 item(s)
   * CldShadeOth0 -> 4 item(s)
   * AMInstabMt0 -> 12 item(s)
   * MorningBound1 -> 336 item(s)
   * SynForcng1 -> 11 item(s)
   * VISCloudCov1 -> 16 item(s)
   * QGVertMotion3 -> 16 item(s)
   * QGVertMotion2 -> 16 item(s)
   * RHRatio2 -> 11 item(s)
   * AreaMoDryAir2 -> 4 item(s)
   * AreaMeso_ALS3 -> 1 item(s)
   * CurPropConv1 -> 297 item(s)
   * CombClouds2 -> 16 item(s)
   * IRCloudCover0 -> 48 item(s)
   * CapChange2 -> 99 item(s)
   * CombMoisture0 -> 4 item(s)
   * Scenario4 -> 9 item(s)
   * Scenario3 -> 9 item(s)
   * IRCloudCover2 -> 48 item(s)
   * Boundaries1 -> 144 item(s)
   * MeanRH2 -> 10 item(s)
   * CombClouds0 -> 16 item(s)
   * AMDewptCalPl1 -> 99 item(s)
   * ScenRel3_44 -> 18 item(s)
   * CldShadeOth2 -> 4 item(s)
   * Dewpoints1 -> 1 item(s)
   * AMInstabMt2 -> 12 item(s)
   * ScenRelAMIns5 -> 108 item(s)
   * WindFieldPln5 -> 7 item(s)
   * Date5 -> 9 item(s)
   * InsInMt1 -> 12 item(s)
   * MountainFcst0 -> 3 item(s)
   * WndHodograph1 -> 36 item(s)
   * AMInstabMt1 -> 12 item(s)
   * N34StarFcst0 -> 33 item(s)
   * MvmtFeatures2 -> 11 item(s)
   * WndHodograph2 -> 36 item(s)
   * WndHodograph3 -> 36 item(s)
   * ScnRelPlFcst7 -> 108 item(s)
   * RaoContMoist2 -> 4 item(s)
   * MeanRH0 -> 10 item(s)
   * Date1 -> 9 item(s)
   * Scenario2 -> 9 item(s)
   * Boundaries2 -> 144 item(s)
   * CapInScen1 -> 99 item(s)
   * AreaMeso_ALS2 -> 1 item(s)
   * Dewpoints0 -> 1 item(s)
   * MeanRH1 -> 10 item(s)
   * CldShadeConv2 -> 72 item(s)
   * CompPlFcst0 -> 3 item(s)
   * CompPlFcst1 -> 3 item(s)
   * ScenRelAMIns2 -> 108 item(s)
   * CapInScen2 -> 99 item(s)
   * CompPlFcst2 -> 3 item(s)
   * Scenario1 -> 9 item(s)
   * WindFieldMt0 -> 11 item(s)
   * Scenario5 -> 9 item(s)
   * OutflowFrMt2 -> 96 item(s)
   * AMCINInScen0 -> 297 item(s)
   * RaoContMoist3 -> 4 item(s)
   * SynForcng0 -> 11 item(s)
   * AreaMeso_ALS1 -> 1 item(s)
   * LatestCIN3 -> 297 item(s)
   * ScnRelPlFcst1 -> 108 item(s)
   * PlainsFcst2 -> 33 item(s)
   * Date4 -> 9 item(s)
   * init -> 1 item(s)
   * ScenRelAMIns0 -> 324 item(s)
   * Scenario0 -> 9 item(s)
   * MorningCIN3 -> 297 item(s)
   * InsInMt2 -> 12 item(s)
   * SfcWndShfDis2 -> 11 item(s)
   * RHRatio0 -> 9 item(s)
   * Scenario6 -> 9 item(s)
   * N0_7muVerMo2 -> 1 item(s)
   * AMCINInScen2 -> 297 item(s)
   * Date0 -> 9 item(s)
   * Scenario7 -> 9 item(s)
   * Scenario9 -> 9 item(s)
   * WndHodograph0 -> 36 item(s)
   * LIfr12ZDENSd0 -> 297 item(s)
   * LIfr12ZDENSd3 -> 297 item(s)
   * ScenRelAMIns1 -> 432 item(s)
   * ScenRelAMIns3 -> 108 item(s)
   * N0_7muVerMo0 -> 1 item(s)
   * AMInsWliScen0 -> 99 item(s)
   * AMInsWliScen1 -> 99 item(s)
   * CldShadeConv1 -> 72 item(s)
   * MorningCIN1 -> 297 item(s)
   * AMInsWliScen2 -> 99 item(s)
   * LoLevMoistAd0 -> 297 item(s)
   * ScenRelAMCIN1 -> 243 item(s)
   * MountainFcst1 -> 3 item(s)
   * LoLevMoistAd1 -> 297 item(s)
   * ScnRelPlFcst5 -> 108 item(s)
   * OutflowFrMt0 -> 144 item(s)
   * LoLevMoistAd3 -> 297 item(s)
   * InsChange1 -> 297 item(s)
   * TempDis0 -> 11 item(s)
   * InsChange2 -> 297 item(s)
   * InsChange0 -> 297 item(s)
   * InsSclInScen1 -> 99 item(s)
   * IRCloudCover1 -> 48 item(s)
   * InsSclInScen2 -> 99 item(s)
   * MorningCIN0 -> 297 item(s)
   * MorningCIN2 -> 297 item(s)
   * SfcWndShfDis0 -> 10 item(s)
   * WindFieldPln0 -> 11 item(s)
   * ScenRelAMIns4 -> 108 item(s)
   * AMCINInScen1 -> 297 item(s)
   * Date3 -> 9 item(s)
   * CapInScen0 -> 99 item(s)
   * VISCloudCov2 -> 16 item(s)
   * LatestCIN0 -> 297 item(s)
   * LIfr12ZDENSd2 -> 297 item(s)
   * LatestCIN1 -> 297 item(s)
   * LatestCIN2 -> 297 item(s)
   * SubjVertMo3 -> 4 item(s)
   * Dewpoints6 -> 1 item(s)
   * LLIW1 -> 1188 item(s)
   * LowLLapse3 -> 10 item(s)
   * ScnRelPlFcst9 -> 108 item(s)
   * MidLLapse0 -> 10 item(s)
   * LLIW2 -> 1188 item(s)
   * CurPropConv0 -> 297 item(s)
   * CurPropConv2 -> 297 item(s)
   * CurPropConv3 -> 297 item(s)
   * LIfr12ZDENSd1 -> 297 item(s)
   * ScnRelPlFcst0 -> 108 item(s)
   * ScenRel3_40 -> 45 item(s)
   * SynForcng2 -> 10 item(s)
   * ScnRelPlFcst2 -> 108 item(s)
   * WindFieldPln4 -> 10 item(s)
   * CldShadeConv0 -> 108 item(s)
   * ScnRelPlFcst3 -> 108 item(s)
   * ScnRelPlFcst4 -> 108 item(s)
   * ScnRelPlFcst6 -> 108 item(s)
   * PlainsFcst0 -> 33 item(s)
   * PlainsFcst1 -> 33 item(s)
   * ScenRel3_43 -> 18 item(s)
   * N34StarFcst2 -> 33 item(s)
   * R5Fcst1 -> 11 item(s)
   * SatContMoist2 -> 16 item(s)
   * SfcWndShfDis6 -> 11 item(s)
   * R5Fcst2 -> 11 item(s)
   * WindFieldPln1 -> 10 item(s)
   * CapChange1 -> 99 item(s)
   * WindFieldPln2 -> 11 item(s)
   * SynForcng3 -> 11 item(s)
   * CombVerMo0 -> 1 item(s)
   * WindFieldPln3 -> 10 item(s)
   * RHRatio1 -> 9 item(s)
   * WindFieldMt1 -> 11 item(s)
   * CldShadeOth1 -> 4 item(s)
   * CombVerMo3 -> 1 item(s)
   * TempDis2 -> 10 item(s)
   * LLIW3 -> 1188 item(s)
   * InsSclInScen0 -> 99 item(s)
   * MountainFcst2 -> 3 item(s)
   * InsInMt0 -> 12 item(s)
   * TempDis3 -> 11 item(s)
   * WindAloft1 -> 9 item(s)
   * WindAloft2 -> 9 item(s)
   * WindAloft3 -> 11 item(s)
   * MvmtFeatures0 -> 10 item(s)
   * MvmtFeatures1 -> 10 item(s)
   * MvmtFeatures3 -> 10 item(s)
   * SfcWndShfDis1 -> 11 item(s)
   * SfcWndShfDis4 -> 9 item(s)
   * SfcWndShfDis5 -> 11 item(s)
   * LowLLapse0 -> 10 item(s)
   * LowLLapse1 -> 11 item(s)
   * LowLLapse2 -> 11 item(s)
   * deadlock -> 7 item(s)
   * Dewpoints2 -> 1 item(s)
   * Dewpoints3 -> 1 item(s)
   * Dewpoints5 -> 1 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Time for model simplification: 4.490s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	2224
Transitions: 	24454
Reward Models:  none
State Labels: 	226 labels
   * WndHodograph2 -> 0 item(s)
   * WindFieldPln3 -> 0 item(s)
   * ScnRelPlFcst4 -> 108 item(s)
   * SatContMoist3 -> 0 item(s)
   * N0_7muVerMo3 -> 0 item(s)
   * WindFieldMt0 -> 0 item(s)
   * Scenario1 -> 0 item(s)
   * CompPlFcst2 -> 0 item(s)
   * TempDis1 -> 0 item(s)
   * WindAloft1 -> 0 item(s)
   * AreaMoDryAir0 -> 0 item(s)
   * LoLevMoistAd2 -> 0 item(s)
   * WindAloft3 -> 0 item(s)
   * LowLLapse1 -> 1 item(s)
   * SfcWndShfDis1 -> 10 item(s)
   * SfcWndShfDis0 -> 9 item(s)
   * MorningCIN2 -> 0 item(s)
   * CombMoisture1 -> 0 item(s)
   * IRCloudCover0 -> 0 item(s)
   * ScenRelAMIns1 -> 0 item(s)
   * InsChange2 -> 0 item(s)
   * VISCloudCov0 -> 0 item(s)
   * QGVertMotion3 -> 0 item(s)
   * QGVertMotion2 -> 0 item(s)
   * SynForcng4 -> 9 item(s)
   * ScnRelPlFcst8 -> 108 item(s)
   * MidLLapse2 -> 0 item(s)
   * Boundaries0 -> 0 item(s)
   * LatestCIN2 -> 0 item(s)
   * SubjVertMo3 -> 0 item(s)
   * Dewpoints6 -> 1 item(s)
   * SubjVertMo1 -> 0 item(s)
   * PlainsFcst0 -> 1 item(s)
   * SfcWndShfDis4 -> 8 item(s)
   * N34StarFcst1 -> 1 item(s)
   * OutflowFrMt1 -> 0 item(s)
   * SubjVertMo0 -> 0 item(s)
   * LLIW0 -> 1 item(s)
   * WndHodograph3 -> 0 item(s)
   * MorningCIN0 -> 0 item(s)
   * MorningBound2 -> 0 item(s)
   * ScnRelPlFcst10 -> 0 item(s)
   * ScnRelPlFcst6 -> 108 item(s)
   * ScnRelPlFcst3 -> 108 item(s)
   * WindAloft2 -> 0 item(s)
   * ScnRelPlFcst2 -> 108 item(s)
   * LoLevMoistAd3 -> 0 item(s)
   * AMCINInScen2 -> 198 item(s)
   * Date0 -> 9 item(s)
   * ScnRelPlFcst0 -> 108 item(s)
   * ScenRel3_40 -> 0 item(s)
   * SynForcng2 -> 8 item(s)
   * deadlock -> 2 item(s)
   * Scenario4 -> 0 item(s)
   * Scenario3 -> 0 item(s)
   * SatContMoist1 -> 0 item(s)
   * SynForcng0 -> 9 item(s)
   * AMCINInScen0 -> 198 item(s)
   * RaoContMoist3 -> 0 item(s)
   * VISCloudCov1 -> 0 item(s)
   * RaoContMoist1 -> 0 item(s)
   * RaoContMoist0 -> 0 item(s)
   * RHRatio1 -> 0 item(s)
   * R5Fcst2 -> 0 item(s)
   * QGVertMotion1 -> 0 item(s)
   * MidLLapse1 -> 0 item(s)
   * AreaMoDryAir3 -> 0 item(s)
   * QGVertMotion0 -> 0 item(s)
   * Dewpoints4 -> 1 item(s)
   * ScnRelPlFcst9 -> 108 item(s)
   * MidLLapse0 -> 0 item(s)
   * LowLLapse0 -> 9 item(s)
   * PlainsFcst1 -> 1 item(s)
   * Scenario5 -> 0 item(s)
   * OutflowFrMt2 -> 0 item(s)
   * N34StarFcst2 -> 0 item(s)
   * N0_7muVerMo1 -> 0 item(s)
   * Scenario9 -> 0 item(s)
   * N0_7muVerMo0 -> 0 item(s)
   * ScenRelAMIns3 -> 0 item(s)
   * WndHodograph0 -> 0 item(s)
   * MvmtFeatures3 -> 0 item(s)
   * TempDis3 -> 0 item(s)
   * N34StarFcst0 -> 1 item(s)
   * MvmtFeatures2 -> 0 item(s)
   * WndHodograph1 -> 0 item(s)
   * SatContMoist2 -> 0 item(s)
   * SfcWndShfDis6 -> 10 item(s)
   * R5Fcst1 -> 1 item(s)
   * MvmtFeatures1 -> 0 item(s)
   * MvmtFeatures0 -> 0 item(s)
   * SfcWndShfDis3 -> 9 item(s)
   * Scenario10 -> 0 item(s)
   * LLIW3 -> 0 item(s)
   * InsSclInScen0 -> 0 item(s)
   * MountainFcst2 -> 3 item(s)
   * InsInMt0 -> 0 item(s)
   * TempDis2 -> 0 item(s)
   * SfcWndShfDis5 -> 10 item(s)
   * Date5 -> 9 item(s)
   * LoLevMoistAd0 -> 0 item(s)
   * MountainFcst0 -> 3 item(s)
   * MountainFcst1 -> 3 item(s)
   * ScenRelAMCIN1 -> 0 item(s)
   * ScenRelAMIns5 -> 0 item(s)
   * AMInstabMt2 -> 0 item(s)
   * WindFieldPln5 -> 0 item(s)
   * Scenario0 -> 0 item(s)
   * MorningCIN3 -> 0 item(s)
   * InsInMt2 -> 0 item(s)
   * SfcWndShfDis2 -> 10 item(s)
   * RHRatio0 -> 0 item(s)
   * LoLevMoistAd1 -> 0 item(s)
   * MeanRH0 -> 0 item(s)
   * OutflowFrMt0 -> 0 item(s)
   * ScnRelPlFcst5 -> 108 item(s)
   * ScnRelPlFcst7 -> 104 item(s)
   * RaoContMoist2 -> 0 item(s)
   * ScenRel3_43 -> 0 item(s)
   * LowLLapse3 -> 9 item(s)
   * LLIW1 -> 0 item(s)
   * LIfr12ZDENSd2 -> 0 item(s)
   * LatestCIN0 -> 0 item(s)
   * LLIW2 -> 0 item(s)
   * Dewpoints5 -> 1 item(s)
   * LIfr12ZDENSd3 -> 0 item(s)
   * IRCloudCover1 -> 0 item(s)
   * InsSclInScen1 -> 0 item(s)
   * InsInMt1 -> 0 item(s)
   * WindFieldMt1 -> 0 item(s)
   * CombVerMo3 -> 0 item(s)
   * CldShadeOth1 -> 0 item(s)
   * LatestCIN1 -> 0 item(s)
   * CombMoisture0 -> 0 item(s)
   * CapChange2 -> 0 item(s)
   * CldShadeOth2 -> 0 item(s)
   * Dewpoints1 -> 1 item(s)
   * AreaMoDryAir2 -> 0 item(s)
   * RHRatio2 -> 0 item(s)
   * MeanRH1 -> 0 item(s)
   * Dewpoints0 -> 1 item(s)
   * CapInScen1 -> 0 item(s)
   * AreaMeso_ALS2 -> 0 item(s)
   * ScnRelPlFcst1 -> 108 item(s)
   * PlainsFcst2 -> 0 item(s)
   * AreaMeso_ALS1 -> 0 item(s)
   * LatestCIN3 -> 0 item(s)
   * AMCINInScen1 -> 297 item(s)
   * Date3 -> 9 item(s)
   * Date1 -> 9 item(s)
   * Scenario2 -> 0 item(s)
   * Scenario8 -> 0 item(s)
   * LIfr12ZDENSd1 -> 0 item(s)
   * CurPropConv3 -> 0 item(s)
   * CurPropConv2 -> 0 item(s)
   * AreaMeso_ALS3 -> 0 item(s)
   * CurPropConv1 -> 0 item(s)
   * ScenRelAMIns2 -> 0 item(s)
   * CapInScen2 -> 0 item(s)
   * CompPlFcst1 -> 0 item(s)
   * CombMoisture2 -> 0 item(s)
   * target -> 1 item(s)
   * InsSclInScen2 -> 0 item(s)
   * WindFieldPln2 -> 0 item(s)
   * SynForcng3 -> 9 item(s)
   * CombVerMo0 -> 0 item(s)
   * ScenRel3_41 -> 1 item(s)
   * CombMoisture3 -> 0 item(s)
   * WindFieldPln1 -> 1 item(s)
   * CapChange1 -> 0 item(s)
   * Scenario7 -> 0 item(s)
   * LowLLapse2 -> 10 item(s)
   * CombClouds2 -> 1 item(s)
   * CombClouds1 -> 1 item(s)
   * ScenRel3_44 -> 1 item(s)
   * AMDewptCalPl1 -> 0 item(s)
   * CurPropConv0 -> 1 item(s)
   * SynForcng1 -> 9 item(s)
   * AMInstabMt0 -> 0 item(s)
   * MorningBound1 -> 0 item(s)
   * CldShadeOth0 -> 0 item(s)
   * AMInsWliScen2 -> 0 item(s)
   * CldShadeConv2 -> 72 item(s)
   * Dewpoints3 -> 1 item(s)
   * AMInsWliScen0 -> 0 item(s)
   * AMInsWliScen1 -> 0 item(s)
   * CldShadeConv1 -> 72 item(s)
   * IRCloudCover2 -> 0 item(s)
   * MeanRH2 -> 0 item(s)
   * Boundaries1 -> 0 item(s)
   * VISCloudCov2 -> 0 item(s)
   * CapInScen0 -> 0 item(s)
   * ScenRelAMIns0 -> 0 item(s)
   * Date4 -> 9 item(s)
   * init -> 1 item(s)
   * CapChange0 -> 0 item(s)
   * TempDis0 -> 0 item(s)
   * InsChange1 -> 0 item(s)
   * SatContMoist0 -> 0 item(s)
   * Date2 -> 9 item(s)
   * AreaMoDryAir1 -> 0 item(s)
   * WindFieldPln4 -> 0 item(s)
   * CldShadeConv0 -> 108 item(s)
   * MorningCIN1 -> 0 item(s)
   * WindAloft0 -> 0 item(s)
   * ScenRelAMCIN0 -> 0 item(s)
   * AreaMeso_ALS0 -> 0 item(s)
   * Boundaries2 -> 0 item(s)
   * ScenRel3_42 -> 0 item(s)
   * R5Fcst0 -> 0 item(s)
   * CombVerMo2 -> 0 item(s)
   * SubjVertMo2 -> 0 item(s)
   * AMDewptCalPl0 -> 0 item(s)
   * InsChange0 -> 0 item(s)
   * AMInstabMt1 -> 0 item(s)
   * MorningBound0 -> 0 item(s)
   * AMDewptCalPl2 -> 0 item(s)
   * Scenario6 -> 0 item(s)
   * N0_7muVerMo2 -> 0 item(s)
   * CombVerMo1 -> 0 item(s)
   * CompPlFcst0 -> 0 item(s)
   * LIfr12ZDENSd0 -> 0 item(s)
   * WindFieldPln0 -> 0 item(s)
   * ScenRelAMIns4 -> 0 item(s)
   * Dewpoints2 -> 1 item(s)
   * CombClouds0 -> 1 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Time for model preprocessing: 4.491s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	2224
Transitions: 	24454
Reward Models:  none
State Labels: 	226 labels
   * WndHodograph2 -> 0 item(s)
   * WindFieldPln3 -> 0 item(s)
   * ScnRelPlFcst4 -> 108 item(s)
   * SatContMoist3 -> 0 item(s)
   * N0_7muVerMo3 -> 0 item(s)
   * WindFieldMt0 -> 0 item(s)
   * Scenario1 -> 0 item(s)
   * CompPlFcst2 -> 0 item(s)
   * TempDis1 -> 0 item(s)
   * WindAloft1 -> 0 item(s)
   * AreaMoDryAir0 -> 0 item(s)
   * LoLevMoistAd2 -> 0 item(s)
   * WindAloft3 -> 0 item(s)
   * LowLLapse1 -> 1 item(s)
   * SfcWndShfDis1 -> 10 item(s)
   * SfcWndShfDis0 -> 9 item(s)
   * MorningCIN2 -> 0 item(s)
   * CombMoisture1 -> 0 item(s)
   * IRCloudCover0 -> 0 item(s)
   * ScenRelAMIns1 -> 0 item(s)
   * InsChange2 -> 0 item(s)
   * VISCloudCov0 -> 0 item(s)
   * QGVertMotion3 -> 0 item(s)
   * QGVertMotion2 -> 0 item(s)
   * SynForcng4 -> 9 item(s)
   * ScnRelPlFcst8 -> 108 item(s)
   * MidLLapse2 -> 0 item(s)
   * Boundaries0 -> 0 item(s)
   * LatestCIN2 -> 0 item(s)
   * SubjVertMo3 -> 0 item(s)
   * Dewpoints6 -> 1 item(s)
   * SubjVertMo1 -> 0 item(s)
   * PlainsFcst0 -> 1 item(s)
   * SfcWndShfDis4 -> 8 item(s)
   * N34StarFcst1 -> 1 item(s)
   * OutflowFrMt1 -> 0 item(s)
   * SubjVertMo0 -> 0 item(s)
   * LLIW0 -> 1 item(s)
   * WndHodograph3 -> 0 item(s)
   * MorningCIN0 -> 0 item(s)
   * MorningBound2 -> 0 item(s)
   * ScnRelPlFcst10 -> 0 item(s)
   * ScnRelPlFcst6 -> 108 item(s)
   * ScnRelPlFcst3 -> 108 item(s)
   * WindAloft2 -> 0 item(s)
   * ScnRelPlFcst2 -> 108 item(s)
   * LoLevMoistAd3 -> 0 item(s)
   * AMCINInScen2 -> 198 item(s)
   * Date0 -> 9 item(s)
   * ScnRelPlFcst0 -> 108 item(s)
   * ScenRel3_40 -> 0 item(s)
   * SynForcng2 -> 8 item(s)
   * deadlock -> 2 item(s)
   * Scenario4 -> 0 item(s)
   * Scenario3 -> 0 item(s)
   * SatContMoist1 -> 0 item(s)
   * SynForcng0 -> 9 item(s)
   * AMCINInScen0 -> 198 item(s)
   * RaoContMoist3 -> 0 item(s)
   * VISCloudCov1 -> 0 item(s)
   * RaoContMoist1 -> 0 item(s)
   * RaoContMoist0 -> 0 item(s)
   * RHRatio1 -> 0 item(s)
   * R5Fcst2 -> 0 item(s)
   * QGVertMotion1 -> 0 item(s)
   * MidLLapse1 -> 0 item(s)
   * AreaMoDryAir3 -> 0 item(s)
   * QGVertMotion0 -> 0 item(s)
   * Dewpoints4 -> 1 item(s)
   * ScnRelPlFcst9 -> 108 item(s)
   * MidLLapse0 -> 0 item(s)
   * LowLLapse0 -> 9 item(s)
   * PlainsFcst1 -> 1 item(s)
   * Scenario5 -> 0 item(s)
   * OutflowFrMt2 -> 0 item(s)
   * N34StarFcst2 -> 0 item(s)
   * N0_7muVerMo1 -> 0 item(s)
   * Scenario9 -> 0 item(s)
   * N0_7muVerMo0 -> 0 item(s)
   * ScenRelAMIns3 -> 0 item(s)
   * WndHodograph0 -> 0 item(s)
   * MvmtFeatures3 -> 0 item(s)
   * TempDis3 -> 0 item(s)
   * N34StarFcst0 -> 1 item(s)
   * MvmtFeatures2 -> 0 item(s)
   * WndHodograph1 -> 0 item(s)
   * SatContMoist2 -> 0 item(s)
   * SfcWndShfDis6 -> 10 item(s)
   * R5Fcst1 -> 1 item(s)
   * MvmtFeatures1 -> 0 item(s)
   * MvmtFeatures0 -> 0 item(s)
   * SfcWndShfDis3 -> 9 item(s)
   * Scenario10 -> 0 item(s)
   * LLIW3 -> 0 item(s)
   * InsSclInScen0 -> 0 item(s)
   * MountainFcst2 -> 3 item(s)
   * InsInMt0 -> 0 item(s)
   * TempDis2 -> 0 item(s)
   * SfcWndShfDis5 -> 10 item(s)
   * Date5 -> 9 item(s)
   * LoLevMoistAd0 -> 0 item(s)
   * MountainFcst0 -> 3 item(s)
   * MountainFcst1 -> 3 item(s)
   * ScenRelAMCIN1 -> 0 item(s)
   * ScenRelAMIns5 -> 0 item(s)
   * AMInstabMt2 -> 0 item(s)
   * WindFieldPln5 -> 0 item(s)
   * Scenario0 -> 0 item(s)
   * MorningCIN3 -> 0 item(s)
   * InsInMt2 -> 0 item(s)
   * SfcWndShfDis2 -> 10 item(s)
   * RHRatio0 -> 0 item(s)
   * LoLevMoistAd1 -> 0 item(s)
   * MeanRH0 -> 0 item(s)
   * OutflowFrMt0 -> 0 item(s)
   * ScnRelPlFcst5 -> 108 item(s)
   * ScnRelPlFcst7 -> 104 item(s)
   * RaoContMoist2 -> 0 item(s)
   * ScenRel3_43 -> 0 item(s)
   * LowLLapse3 -> 9 item(s)
   * LLIW1 -> 0 item(s)
   * LIfr12ZDENSd2 -> 0 item(s)
   * LatestCIN0 -> 0 item(s)
   * LLIW2 -> 0 item(s)
   * Dewpoints5 -> 1 item(s)
   * LIfr12ZDENSd3 -> 0 item(s)
   * IRCloudCover1 -> 0 item(s)
   * InsSclInScen1 -> 0 item(s)
   * InsInMt1 -> 0 item(s)
   * WindFieldMt1 -> 0 item(s)
   * CombVerMo3 -> 0 item(s)
   * CldShadeOth1 -> 0 item(s)
   * LatestCIN1 -> 0 item(s)
   * CombMoisture0 -> 0 item(s)
   * CapChange2 -> 0 item(s)
   * CldShadeOth2 -> 0 item(s)
   * Dewpoints1 -> 1 item(s)
   * AreaMoDryAir2 -> 0 item(s)
   * RHRatio2 -> 0 item(s)
   * MeanRH1 -> 0 item(s)
   * Dewpoints0 -> 1 item(s)
   * CapInScen1 -> 0 item(s)
   * AreaMeso_ALS2 -> 0 item(s)
   * ScnRelPlFcst1 -> 108 item(s)
   * PlainsFcst2 -> 0 item(s)
   * AreaMeso_ALS1 -> 0 item(s)
   * LatestCIN3 -> 0 item(s)
   * AMCINInScen1 -> 297 item(s)
   * Date3 -> 9 item(s)
   * Date1 -> 9 item(s)
   * Scenario2 -> 0 item(s)
   * Scenario8 -> 0 item(s)
   * LIfr12ZDENSd1 -> 0 item(s)
   * CurPropConv3 -> 0 item(s)
   * CurPropConv2 -> 0 item(s)
   * AreaMeso_ALS3 -> 0 item(s)
   * CurPropConv1 -> 0 item(s)
   * ScenRelAMIns2 -> 0 item(s)
   * CapInScen2 -> 0 item(s)
   * CompPlFcst1 -> 0 item(s)
   * CombMoisture2 -> 0 item(s)
   * target -> 1 item(s)
   * InsSclInScen2 -> 0 item(s)
   * WindFieldPln2 -> 0 item(s)
   * SynForcng3 -> 9 item(s)
   * CombVerMo0 -> 0 item(s)
   * ScenRel3_41 -> 1 item(s)
   * CombMoisture3 -> 0 item(s)
   * WindFieldPln1 -> 1 item(s)
   * CapChange1 -> 0 item(s)
   * Scenario7 -> 0 item(s)
   * LowLLapse2 -> 10 item(s)
   * CombClouds2 -> 1 item(s)
   * CombClouds1 -> 1 item(s)
   * ScenRel3_44 -> 1 item(s)
   * AMDewptCalPl1 -> 0 item(s)
   * CurPropConv0 -> 1 item(s)
   * SynForcng1 -> 9 item(s)
   * AMInstabMt0 -> 0 item(s)
   * MorningBound1 -> 0 item(s)
   * CldShadeOth0 -> 0 item(s)
   * AMInsWliScen2 -> 0 item(s)
   * CldShadeConv2 -> 72 item(s)
   * Dewpoints3 -> 1 item(s)
   * AMInsWliScen0 -> 0 item(s)
   * AMInsWliScen1 -> 0 item(s)
   * CldShadeConv1 -> 72 item(s)
   * IRCloudCover2 -> 0 item(s)
   * MeanRH2 -> 0 item(s)
   * Boundaries1 -> 0 item(s)
   * VISCloudCov2 -> 0 item(s)
   * CapInScen0 -> 0 item(s)
   * ScenRelAMIns0 -> 0 item(s)
   * Date4 -> 9 item(s)
   * init -> 1 item(s)
   * CapChange0 -> 0 item(s)
   * TempDis0 -> 0 item(s)
   * InsChange1 -> 0 item(s)
   * SatContMoist0 -> 0 item(s)
   * Date2 -> 9 item(s)
   * AreaMoDryAir1 -> 0 item(s)
   * WindFieldPln4 -> 0 item(s)
   * CldShadeConv0 -> 108 item(s)
   * MorningCIN1 -> 0 item(s)
   * WindAloft0 -> 0 item(s)
   * ScenRelAMCIN0 -> 0 item(s)
   * AreaMeso_ALS0 -> 0 item(s)
   * Boundaries2 -> 0 item(s)
   * ScenRel3_42 -> 0 item(s)
   * R5Fcst0 -> 0 item(s)
   * CombVerMo2 -> 0 item(s)
   * SubjVertMo2 -> 0 item(s)
   * AMDewptCalPl0 -> 0 item(s)
   * InsChange0 -> 0 item(s)
   * AMInstabMt1 -> 0 item(s)
   * MorningBound0 -> 0 item(s)
   * AMDewptCalPl2 -> 0 item(s)
   * Scenario6 -> 0 item(s)
   * N0_7muVerMo2 -> 0 item(s)
   * CombVerMo1 -> 0 item(s)
   * CompPlFcst0 -> 0 item(s)
   * LIfr12ZDENSd0 -> 0 item(s)
   * WindFieldPln0 -> 0 item(s)
   * ScenRelAMIns4 -> 0 item(s)
   * Dewpoints2 -> 1 item(s)
   * CombClouds0 -> 1 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 
Parameters: p509 p510 p511 p401 p437 p473 p413 p425 p449 p461 p485 p497 p402 p438 p474 p414 p426 p450 p462 p486 p498 p403 p439 p475 p415 p427 p451 p463 p487 p499 p404 p440 p476 p416 p428 p452 p464 p488 p500 p405 p441 p477 p417 p429 p453 p465 p489 p501 p406 p442 p478 p418 p430 p454 p466 p490 p502 p407 p443 p479 p419 p431 p455 p467 p491 p503 p408 p444 p480 p420 p432 p456 p468 p492 p504 p409 p445 p481 p421 p433 p457 p469 p493 p505 p410 p446 p482 p422 p434 p458 p470 p494 p506 p411 p447 p483 p423 p435 p459 p471 p495 p507 p412 p448 p484 p424 p436 p460 p472 p496 p508 p393 p387 p388 p389 p390 p391 p392 p395 p398 p394 p397 p400 p396 p399 p28 p117 p207 p297 p37 p127 p217 p307 p47 p137 p227 p317 p57 p147 p237 p327 p67 p157 p247 p337 p77 p167 p257 p347 p87 p177 p267 p357 p97 p187 p277 p367 p107 p197 p287 p377 p29 p118 p208 p298 p38 p128 p218 p308 p48 p138 p228 p318 p58 p148 p238 p328 p68 p158 p248 p338 p78 p168 p258 p348 p88 p178 p268 p358 p98 p188 p278 p368 p108 p198 p288 p378 p30 p119 p209 p299 p39 p129 p219 p309 p49 p139 p229 p319 p59 p149 p239 p329 p69 p159 p249 p339 p79 p169 p259 p349 p89 p179 p269 p359 p99 p189 p279 p369 p109 p199 p289 p379 p31 p120 p210 p300 p40 p130 p220 p310 p50 p140 p230 p320 p60 p150 p240 p330 p70 p160 p250 p340 p80 p170 p260 p350 p90 p180 p270 p360 p100 p190 p280 p370 p110 p200 p290 p380 p32 p121 p211 p301 p41 p131 p221 p311 p51 p141 p231 p321 p61 p151 p241 p331 p71 p161 p251 p341 p81 p171 p261 p351 p91 p181 p271 p361 p101 p191 p281 p371 p111 p201 p291 p381 p33 p122 p212 p302 p42 p132 p222 p312 p52 p142 p232 p322 p62 p152 p242 p332 p72 p162 p252 p342 p82 p172 p262 p352 p92 p182 p272 p362 p102 p192 p282 p372 p112 p202 p292 p382 p34 p123 p213 p303 p43 p133 p223 p313 p53 p143 p233 p323 p63 p153 p243 p333 p73 p163 p253 p343 p83 p173 p263 p353 p93 p183 p273 p363 p103 p193 p283 p373 p113 p203 p293 p383 p124 p214 p304 p44 p134 p224 p314 p54 p144 p234 p324 p64 p154 p244 p334 p74 p164 p254 p344 p84 p174 p264 p354 p94 p184 p274 p364 p104 p194 p284 p374 p114 p204 p294 p384 p35 p125 p215 p305 p45 p135 p225 p315 p55 p145 p235 p325 p65 p155 p245 p335 p75 p165 p255 p345 p85 p175 p265 p355 p95 p185 p275 p365 p105 p195 p285 p375 p115 p205 p295 p385 p36 p126 p216 p306 p46 p136 p226 p316 p56 p146 p236 p326 p66 p156 p246 p336 p76 p166 p256 p346 p86 p176 p266 p356 p96 p186 p276 p366 p106 p196 p286 p376 p116 p206 p296 p386 p19 p20 p21 p22 p23 p24 p25 p26 p27 p9 p10 p11 p12 p13 p14 p15 p16 p17 p18 p0 p1 p2 p3 p4 p5 p6 p7 p8 
Finding a feasible instantiation using Gradient Descent
Trying out a new starting point
Trying initial guess (p->0.5 for every parameter p or set start point)
Currently at 0.9104766241
Currently at 0.7924776505
Currently at 0.6166758175
Currently at 0.3592101687
Currently at 0.3284273721
Currently at 0.3083371287
Currently at 0.2856716781
Currently at 0.2785985992
Currently at 0.2785886764
Currently at 0.278578657
Currently at 0.278555502
Currently at 0.2785476377
Currently at 0.2785420326
Currently at 0.2785325179
Currently at 0.2785291472
Currently at 0.2785254238
Currently at 0.2785214357
Currently at 0.2785158893
Sorry, couldn't satisfy the bound (yet). Best found value so far: 0.2785151182
Trying out a new starting point
Currently at 0.9601283354
Currently at 0.8641007616
Currently at 0.7210681592
Currently at 0.4594334254
Currently at 0.2913148085
Currently at 0.2790923418
Currently at 0.2790372979
Currently at 0.2789241422
Currently at 0.2786354145
Currently at 0.2786258278
Currently at 0.2786106195
Currently at 0.2785784191
Currently at 0.278562926
Currently at 0.2785593451
Currently at 0.2785537496
Currently at 0.2785515618
Currently at 0.2785472315
Currently at 0.2785438296
Currently at 0.2785416855
Currently at 0.2785393591
Currently at 0.278534234
Currently at 0.2785314671
Currently at 0.2785286554
Currently at 0.2785226793
Currently at 0.2785200687
Sorry, couldn't satisfy the bound (yet). Best found value so far: 0.2785151182
Trying out a new starting point
Currently at 0.9092736809
Currently at 0.8603703986
Currently at 0.7990256242
Currently at 0.7322112575
Currently at 0.5900250349
Currently at 0.4352811009
Currently at 0.3151367554
Currently at 0.2789556705
Currently at 0.2787618998
Currently at 0.2786376772
Currently at 0.2785964283
Currently at 0.278582227
Currently at 0.2785717647
