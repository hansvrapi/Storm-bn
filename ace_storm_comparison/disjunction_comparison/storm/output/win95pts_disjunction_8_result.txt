Storm 1.6.4

Date: Tue Apr 26 11:05:10 2022
Command line arguments: --jani storm/win95pts/win95pts.jani --prop 'P=? [F((AppDtGnTm=0|PrntPrcssTm=1|PrtMem=0|DeskPrntSpd=0|NtSpd=1|NnPSGrphc=1|IncmpltPS=0|Problem3=1))]'
Current working directory: /experiments/ace_storm_comparison/disjunction_comparison

Time for model input parsing: 0.053s.

Time for model construction: 0.117s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	4824
Transitions: 	8709
Reward Models:  none
State Labels: 	3 labels
   * deadlock -> 2 item(s)
   * init -> 1 item(s)
   * ((((((((AppDtGnTm = 0) | (PrntPrcssTm = 1)) | (PrtMem = 0)) | (DeskPrntSpd = 0)) | (NtSpd = 1)) | (NnPSGrphc = 1)) | (IncmpltPS = 0)) | (Problem3 = 1)) -> 24 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Model checking property "1": P=? [F ((((((((AppDtGnTm = 0) | (PrntPrcssTm = 1)) | (PrtMem = 0)) | (DeskPrntSpd = 0)) | (NtSpd = 1)) | (NnPSGrphc = 1)) | (IncmpltPS = 0)) | (Problem3 = 1))] ...
Result (for initial states): 0.9999999662
Time for model checking: 0.003s.
