Storm 1.6.4

Date: Tue Apr 26 11:01:40 2022
Command line arguments: --jani storm/hailfinder/hailfinder.jani --prop 'P=? [F((VISCloudCov=0|CldShadeOth=0|LoLevMoistAd=0|InsChange=1|CapChange=0|MorningCIN=0|AMCINInScen=0|CurPropConv=0))]'
Current working directory: /experiments/ace_storm_comparison/disjunction_comparison

Time for model input parsing: 0.111s.

Time for model construction: 0.286s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	18419
Transitions: 	49685
Reward Models:  none
State Labels: 	3 labels
   * deadlock -> 7 item(s)
   * init -> 1 item(s)
   * ((((((((VISCloudCov = 0) | (CldShadeOth = 0)) | (LoLevMoistAd = 0)) | (InsChange = 1)) | (CapChange = 0)) | (MorningCIN = 0)) | (AMCINInScen = 0)) | (CurPropConv = 0)) -> 1406 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Model checking property "1": P=? [F ((((((((VISCloudCov = 0) | (CldShadeOth = 0)) | (LoLevMoistAd = 0)) | (InsChange = 1)) | (CapChange = 0)) | (MorningCIN = 0)) | (AMCINInScen = 0)) | (CurPropConv = 0))] ...
Result (for initial states): 0.8662004042
Time for model checking: 0.007s.
Storm 1.6.4

Date: Tue Apr 26 11:02:25 2022
Command line arguments: --jani storm/hailfinder/hailfinder.jani --prop 'P=? [F((VISCloudCov=0|CldShadeOth=0|LoLevMoistAd=0|InsChange=1|CapChange=0|MorningCIN=0|AMCINInScen=0|CurPropConv=0))]'
Current working directory: /experiments/ace_storm_comparison/disjunction_comparison

Time for model input parsing: 0.109s.

Time for model construction: 0.287s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	18419
Transitions: 	49685
Reward Models:  none
State Labels: 	3 labels
   * deadlock -> 7 item(s)
   * init -> 1 item(s)
   * ((((((((VISCloudCov = 0) | (CldShadeOth = 0)) | (LoLevMoistAd = 0)) | (InsChange = 1)) | (CapChange = 0)) | (MorningCIN = 0)) | (AMCINInScen = 0)) | (CurPropConv = 0)) -> 1406 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 

Model checking property "1": P=? [F ((((((((VISCloudCov = 0) | (CldShadeOth = 0)) | (LoLevMoistAd = 0)) | (InsChange = 1)) | (CapChange = 0)) | (MorningCIN = 0)) | (AMCINInScen = 0)) | (CurPropConv = 0))] ...
Result (for initial states): 0.8662004042
Time for model checking: 0.007s.
