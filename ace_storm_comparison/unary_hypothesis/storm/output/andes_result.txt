Storm 1.6.4 (dev)

Date: Thu Apr 28 12:14:26 2022
Command line arguments: --jani storm/jani_files/andes.jani --expvisittimes --build-all-labels
Current working directory: /home/hans/Desktop/private/ace_storm_comparison/unary_hypothesis

Time for model input parsing: 0.068s.

Time for model construction: 85.529s.

-------------------------------------------------------------- 
Model type: 	DTMC (sparse)
States: 	10889443
Transitions: 	20933804
Reward Models:  none
State Labels: 	2 labels
   * deadlock -> 2 item(s)
   * init -> 1 item(s)
Choice Labels: 	none
-------------------------------------------------------------- 
Accumulated values for labels:
	deadlock: inf
	init: 1
Result: [2.681780257e-20, inf] (range)
Time for model checking: 3.472s.
